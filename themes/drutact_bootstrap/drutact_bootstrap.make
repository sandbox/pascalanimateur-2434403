api = 2
core = 7

; Bootstrap
projects[bootstrap][version] = 3.1

; Bootstrap 3.3.5 library
libraries[bootstrap][download][type] = file
libraries[bootstrap][download][url] = https://github.com/twbs/bootstrap/archive/v3.3.5.zip
libraries[bootstrap][destination] = ../../profiles/drutact/themes/drutact_bootstrap

; Smartmenus
libraries[smartmenus][download][type] = file
libraries[smartmenus][download][url] = https://github.com/vadikom/smartmenus/archive/master.zip
libraries[smartmenus][destination] = ../../profiles/drutact/themes/drutact_bootstrap
libraries[smartmenus][download][subtree] = smartmenus-master/dist
