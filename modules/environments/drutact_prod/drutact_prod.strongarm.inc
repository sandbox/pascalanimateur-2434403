<?php
/**
 * @file
 * drutact_prod.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drutact_prod_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'captcha_default_challenge';
  $strongarm->value = 'hidden_captcha/Hidden CAPTCHA';
  $export['captcha_default_challenge'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'captcha_default_challenge_on_nonlisted_forms';
  $strongarm->value = 1;
  $export['captcha_default_challenge_on_nonlisted_forms'] = $strongarm;

  return $export;
}
