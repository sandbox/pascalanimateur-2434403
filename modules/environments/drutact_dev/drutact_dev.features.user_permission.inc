<?php
/**
 * @file
 * drutact_dev.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_dev_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access devel information'.
  $permissions['access devel information'] = array(
    'name' => 'access devel information',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'devel',
  );

  // Exported permission: 'administer field_library'.
  $permissions['administer field_library'] = array(
    'name' => 'administer field_library',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'field_library',
  );

  // Exported permission: 'execute php code'.
  $permissions['execute php code'] = array(
    'name' => 'execute php code',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'devel',
  );

  // Exported permission: 'reset drupal'.
  $permissions['reset drupal'] = array(
    'name' => 'reset drupal',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'drupal_reset',
  );

  // Exported permission: 'submit translations to localization server'.
  $permissions['submit translations to localization server'] = array(
    'name' => 'submit translations to localization server',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'l10n_client',
  );

  // Exported permission: 'switch users'.
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'devel',
  );

  // Exported permission: 'use field_library'.
  $permissions['use field_library'] = array(
    'name' => 'use field_library',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'field_library',
  );

  // Exported permission: 'use on-page translation'.
  $permissions['use on-page translation'] = array(
    'name' => 'use on-page translation',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'l10n_client',
  );

  return $permissions;
}
