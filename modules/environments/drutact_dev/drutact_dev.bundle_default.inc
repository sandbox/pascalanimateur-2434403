<?php
/**
 * @file
 * drutact_dev.bundle_default.inc
 */

/**
 * Implements hook_field_library_bundle_defaults().
 */
function drutact_dev_field_library_bundle_defaults() {
  $export = array();

  $field_library_bundle = new stdClass();
  $field_library_bundle->api_version = 1;
  $field_library_bundle->name = 'common';
  $field_library_bundle->title = 'Common';
  $field_library_bundle->description = 'Section
Tags';
  $export['common'] = $field_library_bundle;

  $field_library_bundle = new stdClass();
  $field_library_bundle->api_version = 1;
  $field_library_bundle->name = 'group_content';
  $field_library_bundle->title = 'Group content';
  $field_library_bundle->description = 'Groups audience
Group content visibility';
  $export['group_content'] = $field_library_bundle;

  return $export;
}
