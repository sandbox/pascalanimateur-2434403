<?php
/**
 * @file
 * drutact_dev.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function drutact_dev_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: devel.
  $menus['devel'] = array(
    'menu_name' => 'devel',
    'title' => 'Development',
    'description' => 'Development link',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Development');
  t('Development link');


  return $menus;
}
