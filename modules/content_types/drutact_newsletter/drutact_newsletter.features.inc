<?php
/**
 * @file
 * drutact_newsletter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drutact_newsletter_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function drutact_newsletter_node_info() {
  $items = array(
    'simplenews' => array(
      'name' => t('Newsletter'),
      'base' => 'node_content',
      'description' => t('Use <em>newletters</em> to send information by email to lists of subscribers.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
