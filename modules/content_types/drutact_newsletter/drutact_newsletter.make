api = 2
core = 7

; Simplenews
projects[simplenews][type] = module
projects[simplenews][patch][1564964] = https://www.drupal.org/files/issues/simplenews-fix_multilinguality-1564964-41.patch
projects[simplenews_roles][version] = 1.x-dev
