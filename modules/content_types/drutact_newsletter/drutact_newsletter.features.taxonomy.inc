<?php
/**
 * @file
 * drutact_newsletter.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drutact_newsletter_taxonomy_default_vocabularies() {
  return array(
    'newsletter' => array(
      'name' => 'Newsletter types',
      'machine_name' => 'newsletter',
      'description' => 'Categories used for the management of newsletter subscription lists and content.',
      'hierarchy' => 0,
      'module' => 'simplenews',
      'weight' => 0,
    ),
  );
}
