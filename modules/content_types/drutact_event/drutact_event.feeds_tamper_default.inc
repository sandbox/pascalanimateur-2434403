<?php
/**
 * @file
 * drutact_event.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function drutact_event_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'import_events_xls-author-efq_finder';
  $feeds_tamper->importer = 'import_events_xls';
  $feeds_tamper->source = 'author';
  $feeds_tamper->plugin_id = 'efq_finder';
  $feeds_tamper->settings = array(
    'update' => 'Update',
    'entity_type' => 'user',
    'bundle' => 'user',
    'field' => 'name',
    'property' => TRUE,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Entity Field Query finder';
  $export['import_events_xls-author-efq_finder'] = $feeds_tamper;

  return $export;
}
