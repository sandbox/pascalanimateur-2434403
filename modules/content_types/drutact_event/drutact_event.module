<?php
/**
 * @file
 * Code for the DruTACT - Event feature.
 */

include_once 'drutact_event.features.inc';

/**
 * Implements hook_update_projects_alter().
 */
function drutact_event_update_projects_alter(&$projects)
{
  unset($projects['drutact_event']);
}

/**
 * Implements hook_l10n_update_projects_alter().
 */
function drutact_event_l10n_update_projects_alter(&$projects)
{
  unset($projects['drutact_event']);
}

/**
 * Implements hook_form_alter().
 */
function drutact_event_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['field_event_type'])) {
    $form['field_event_type']['und']['#description'] = t('<a href="@manage-event-types">Manage event types</a> for your site (requires Editor role).', array('@manage-event-types' => url('admin/structure/taxonomy/event_types')));
  }
}

/**
 * Implements hook_panels_display_save().
 */
function drutact_event_panels_display_save($display) {
  if ($display->uuid == '2c7d3767-c7f6-4de0-93e7-c711bbbebd50') {
    $paths = array(
      'events',
    );
    drutact_l10n_update_path_aliases_translation($paths);
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function drutact_event_theme_registry_alter(&$registry) {
  $path = drupal_get_path('module', 'drutact_event') . '/templates';
  $templates = drupal_find_theme_templates($registry, '.tpl.php', $path);
  foreach ($templates as $key => $value) {
    $templates[$key]['type'] = 'module';
  }
  $registry += $templates;
}

/**
 * Implements hook_views_invalidate_cache().
 *
 * Create translated path aliases for views pages.
 */
function drutact_event_views_invalidate_cache() {
  $paths = array(
    'calendar',
    'calendar/month',
    'calendar/week',
    'calendar/day',
    'calendar/year',
  );
  drutact_l10n_update_path_aliases_translation($paths);
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function drutact_event_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if ($root_path == 'admin/content/events') {
    $item = menu_get_item('node/add/event');
    $item['title'] = t('Add event');
    $data['actions']['output'][] = array(
      '#theme' => 'menu_local_action',
      '#link' => $item,
    );
    $item = menu_get_item('import/import_events_xls');
    $item['title'] = t('Import events');
    $data['actions']['output'][] = array(
      '#theme' => 'menu_local_action',
      '#link' => $item,
    );
  }
}
