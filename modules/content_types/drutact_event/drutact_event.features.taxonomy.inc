<?php
/**
 * @file
 * drutact_event.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drutact_event_taxonomy_default_vocabularies() {
  return array(
    'event_types' => array(
      'name' => 'Event types',
      'machine_name' => 'event_types',
      'description' => 'Categories used for the management of event content.',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
