<?php
/**
 * @file
 * drutact_event.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_event_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'clear import_events_xls feeds'.
  $permissions['clear import_events_xls feeds'] = array(
    'name' => 'clear import_events_xls feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'create event content'.
  $permissions['create event content'] = array(
    'name' => 'create event content',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any event content'.
  $permissions['delete any event content'] = array(
    'name' => 'delete any event content',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own event content'.
  $permissions['delete own event content'] = array(
    'name' => 'delete own event content',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in event_types'.
  $permissions['delete terms in event_types'] = array(
    'name' => 'delete terms in event_types',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any event content'.
  $permissions['edit any event content'] = array(
    'name' => 'edit any event content',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own event content'.
  $permissions['edit own event content'] = array(
    'name' => 'edit own event content',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in event_types'.
  $permissions['edit terms in event_types'] = array(
    'name' => 'edit terms in event_types',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'import import_events_xls feeds'.
  $permissions['import import_events_xls feeds'] = array(
    'name' => 'import import_events_xls feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'tamper import_events_xls'.
  $permissions['tamper import_events_xls'] = array(
    'name' => 'tamper import_events_xls',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'unlock import_events_xls feeds'.
  $permissions['unlock import_events_xls feeds'] = array(
    'name' => 'unlock import_events_xls feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
