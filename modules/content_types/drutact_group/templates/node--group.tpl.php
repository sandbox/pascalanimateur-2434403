<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php if (!$page && !empty($title)): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($display_submitted): ?>
        <div class="submitted">
          <?php print $user_picture; ?>
          <?php print $submitted;?>
        </div>
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide($content['field_section']);
  hide($content['field_group_type']);
  hide($content['og_group_ref']);
  hide($content['group_access']);
  hide($content['field_tags']);
  hide($content['links']);
  hide($content['comments']);
  // Render the actual node.
  print render($content);
  ?>
  <?php if (!empty($content['og_group_ref']) || !empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <div class="node-footer">
        <?php print render($content['field_section']); ?>
        <?php print render($content['group_access']); ?>
        <?php print render($content['og_group_ref']); ?>
        <?php print render($content['field_group_type']); ?>
        <?php print render($content['field_tags']); ?>
        <?php print render($content['links']); ?>
      </div>
    </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
