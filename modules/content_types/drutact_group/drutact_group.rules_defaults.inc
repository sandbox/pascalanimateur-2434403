<?php
/**
 * @file
 * drutact_group.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function drutact_group_default_rules_configuration() {
  $items = array();
  $items['rules_group_content_assignment'] = entity_import('rules_config', '{ "rules_group_content_assignment" : {
      "LABEL" : "Group content assignment",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "group" ],
      "REQUIRES" : [ "og" ],
      "USES VARIABLES" : {
        "node" : { "label" : "Node", "type" : "node" },
        "group" : { "label" : "Group", "type" : "node" }
      },
      "IF" : [
        { "og_entity_is_group_content" : { "entity" : [ "node" ] } },
        { "og_entity_is_group" : { "entity" : [ "group" ] } }
      ],
      "DO" : [
        { "og_group_content_add" : { "entity" : [ "node" ], "group" : [ "group" ] } }
      ]
    }
  }');
  $items['rules_user_group_subscription'] = entity_import('rules_config', '{ "rules_user_group_subscription" : {
      "LABEL" : "User group subscription",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Group", "User" ],
      "REQUIRES" : [ "og" ],
      "USES VARIABLES" : {
        "user" : { "label" : "User", "type" : "user" },
        "group" : { "label" : "Group", "type" : "node" }
      },
      "IF" : [ { "og_entity_is_group" : { "entity" : [ "group" ] } } ],
      "DO" : [
        { "og_subcribe_user" : { "user" : [ "user" ], "group" : [ "group" ] } }
      ]
    }
  }');
  return $items;
}
