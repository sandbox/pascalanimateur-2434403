api = 2
core = 7

; Group
projects[og][type] = module
projects[og][patch][2455625] = https://www.drupal.org/files/issues/og-remove_example.patch
projects[og][patch][1473262] = https://www.drupal.org/files/issues/og-translate_roles_display-1473262-6.patch
projects[og][patch][2359383] = https://www.drupal.org/files/issues/og-missing_translations.patch
projects[og][patch][2494575] = https://www.drupal.org/files/issues/og-overview_handler_force_group_type.patch
projects[og][patch][] = https://www.drupal.org/files/issues/og-user_roles_action_force_group_type.patch
projects[og][patch][2494587] = https://www.drupal.org/files/issues/og-overview_show_total_files.patch
projects[og_default_members][type] = module
projects[og_file_entity][type] = module
projects[og_file_entity][download][type] = git
projects[og_file_entity][download][url] = http://git.drupal.org/sandbox/jeffschuler/2091417.git
projects[og_file_entity][download][branch] = 7.x-1.x
projects[og_file_entity][patch][2112373] = https://www.drupal.org/files/issues/og_file_entity-add_view_access-2112373.patch
projects[og_manager_change][type] = module
projects[og_role_access][type] = module
projects[og_role_access][download][type] = git
projects[og_role_access][download][url] = http://git.drupal.org/sandbox/weekbeforenext/1794706.git
projects[og_role_access][download][branch] = 7.x-2.x
projects[group_contact][type] = module
projects[group_contact][download][type] = git
projects[group_contact][download][url] = http://git.drupal.org/sandbox/PascalAnimateur/2554115.git
projects[group_contact][download][branch] = 7.x-1.x
