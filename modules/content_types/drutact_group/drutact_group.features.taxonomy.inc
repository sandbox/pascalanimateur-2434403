<?php
/**
 * @file
 * drutact_group.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drutact_group_taxonomy_default_vocabularies() {
  return array(
    'group_types' => array(
      'name' => 'Group types',
      'machine_name' => 'group_types',
      'description' => 'Categories used for the management of groups.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
