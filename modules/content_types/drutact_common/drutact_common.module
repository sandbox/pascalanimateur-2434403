<?php
/**
 * @file
 * Code for the DruTACT - Common feature.
 */

include_once 'drutact_common.features.inc';
/**
 * @file
 * Code for the DruTACT - Content feature.
 */

/**
 * Implements hook_update_projects_alter().
 */
function drutact_common_update_projects_alter(&$projects)
{
  unset($projects['drutact_common']);
}

/**
 * Implements hook_l10n_update_projects_alter().
 */
function drutact_common_l10n_update_projects_alter(&$projects)
{
  unset($projects['drutact_common']);
}


/**
 * Implements hook_form_alter().
 * Add Image vertical tab to node add/edit forms.
 */
function drutact_common_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['field_image'])) {
    // Make sure additional_settings exists
    if (!isset($form['additional_settings'])) {
      $form['additional_settings'] = array(
        '#type' => 'vertical_tabs',
        '#weight' => 99,
      );
    }

    // Move image field to Content image tab
    $form['content_image_tab'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content image'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#group' => 'additional_settings',
      '#weight' => -10,
      'field_image' => $form['field_image'],
    );
    $form['field_image']['#access'] = FALSE;

    // Move group visibility to Group access vertical tab
    if (isset($form['field_image_style'])) {
      $form['content_image_tab']['field_image_style'] = $form['field_image_style'];
      $form['field_image_style']['#access'] = FALSE;
    }

    // Add custom validation callback needed to fix form values
    if (isset($form['#node_edit_form'])) {
      array_unshift($form['#validate'], '_drutact_common_image_tab_form_validate');
    }
  }
}

/**
 * Validation callback needed for Content image tab on node add/edit form.
 */
function _drutact_common_image_tab_form_validate($form, &$form_state) {
  // Move field_image value from group_access_tab to its original location
  if (isset($form_state['values']['content_image_tab']['field_image'])) {
    $form_state['values']['field_image'] = $form_state['values']['content_image_tab']['field_image'];
    unset($form_state['values']['content_image_tab']['field_image']);
  }
  // Move group_content_access value from group_access_tab to its original location
  if (isset($form_state['values']['content_image_tab']['field_image_style'])) {
    $form_state['values']['field_image_style'] = $form_state['values']['content_image_tab']['field_image_style'];
    unset($form_state['values']['content_image_tab']['field_image_style']);
  }
  return $form;
}
