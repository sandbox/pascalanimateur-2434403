<?php
/**
 * @file
 * drutact_resource.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drutact_resource_taxonomy_default_vocabularies() {
  return array(
    'resource_types' => array(
      'name' => 'Resource types',
      'machine_name' => 'resource_types',
      'description' => 'Categories used for the management of resource content.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
