<?php
/**
 * @file
 * drutact_webform.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drutact_webform_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function drutact_webform_node_info() {
  $items = array(
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>formulaires</em> afin de créer des formulaires en ligne accessibles aux utilisateurs. Les résultats de soumissions et les statistiques enregistrées sont accessibles aux utilisateurs privilégiés.'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => t('Vous devez d\'abord saisir un titre et enregistrer le formulaire avant de pouvoir y ajouter des composants (questions, instructions, etc.)'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
