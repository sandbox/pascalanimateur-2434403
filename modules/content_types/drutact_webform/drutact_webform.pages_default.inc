<?php
/**
 * @file
 * drutact_webform.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function drutact_webform_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'webforms';
  $page->task = 'page';
  $page->admin_title = 'Webforms';
  $page->admin_description = 'Public list of webform types.';
  $page->path = 'webforms';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Webforms',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_webforms__panel_context_6d47c00d-3839-46ea-83d2-9bfc34fdaa8c';
  $handler->task = 'page';
  $handler->subtask = 'webforms';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panneau',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Webforms';
  $display->uuid = 'da12b51e-89de-4047-b0dd-9b64b66896bf';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-371c6a2f-7596-4980-b380-1a8e0126f012';
    $pane->panel = 'top';
    $pane->type = 'views';
    $pane->subtype = 'public_terms';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 1,
      'link_to_view' => 0,
      'args' => 'webform_types',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '371c6a2f-7596-4980-b380-1a8e0126f012';
    $display->content['new-371c6a2f-7596-4980-b380-1a8e0126f012'] = $pane;
    $display->panels['top'][0] = 'new-371c6a2f-7596-4980-b380-1a8e0126f012';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-371c6a2f-7596-4980-b380-1a8e0126f012';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['webforms'] = $page;

  return $pages;

}
