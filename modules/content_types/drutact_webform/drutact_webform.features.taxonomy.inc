<?php
/**
 * @file
 * drutact_webform.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drutact_webform_taxonomy_default_vocabularies() {
  return array(
    'webform_types' => array(
      'name' => 'Webform types',
      'machine_name' => 'webform_types',
      'description' => 'Categories used for the management of webforms.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
