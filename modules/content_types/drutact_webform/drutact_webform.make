api = 2
core = 7

; Webform
projects[webform][type] = module
projects[webform][patch][2358425] = https://www.drupal.org/files/issues/webform-fix_illegal_string_offset_0.patch
projects[webform_add_existing][type] = module
projects[webform_add_existing][version] = 2.0
projects[webform_charts][type] = module
projects[webform_charts][patch][2347065] = https://www.drupal.org/files/issues/webform_charts-no_inline_types.patch
projects[webform_charts][patch][2454389] = https://www.drupal.org/files/issues/webform_charts-fix_empty_analysis_data-2454389-1.patch
projects[webform_import][type] = module
projects[webform_multiple_file][type] = module
projects[webform_scheduler][type] = module
