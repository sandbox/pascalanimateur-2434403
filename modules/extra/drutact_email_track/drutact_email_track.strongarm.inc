<?php
/**
 * @file
 * drutact_email_track.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drutact_email_track_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mail_tracking_statistics_period';
  $strongarm->value = 'week';
  $export['mail_tracking_statistics_period'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mail_tracking_token_retention';
  $strongarm->value = '1 month';
  $export['mail_tracking_token_retention'] = $strongarm;

  return $export;
}
