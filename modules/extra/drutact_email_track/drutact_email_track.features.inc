<?php
/**
 * @file
 * drutact_email_track.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drutact_email_track_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
