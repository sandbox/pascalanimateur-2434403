<?php
/**
 * @file
 * drutact_email_track.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_email_track_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access mail tracking statistics'.
  $permissions['access mail tracking statistics'] = array(
    'name' => 'access mail tracking statistics',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'mail_tracking',
  );

  // Exported permission: 'access mail tracking tokens'.
  $permissions['access mail tracking tokens'] = array(
    'name' => 'access mail tracking tokens',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'mail_tracking',
  );

  // Exported permission: 'administer mail tracking'.
  $permissions['administer mail tracking'] = array(
    'name' => 'administer mail tracking',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'mail_tracking',
  );

  return $permissions;
}
