<?php
/**
 * @file
 * drutact_charts.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_charts_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access example charts'.
  $permissions['access example charts'] = array(
    'name' => 'access example charts',
    'roles' => array(),
    'module' => 'charts',
  );

  // Exported permission: 'administer charts'.
  $permissions['administer charts'] = array(
    'name' => 'administer charts',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'charts',
  );

  return $permissions;
}
