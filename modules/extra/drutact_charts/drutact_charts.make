api = 2
core = 7

; Charts
projects[charts][type] = module

; Highcharts.js library
libraries[highcharts/js][download][type] = file
libraries[highcharts/js][download][url] = http://code.highcharts.com/highcharts.js
libraries[highcharts/js][destination] = "../../sites/all/libraries"
