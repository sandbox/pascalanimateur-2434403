<?php
/**
 * @file
 * drutact_email_log.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drutact_email_log_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mail_logger_log_maximum_age';
  $strongarm->value = '0';
  $export['mail_logger_log_maximum_age'] = $strongarm;

  return $export;
}
