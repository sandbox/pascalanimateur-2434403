<?php
/**
 * @file
 * drutact_email_log.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_email_log_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access mail logger'.
  $permissions['access mail logger'] = array(
    'name' => 'access mail logger',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'mail_logger',
  );

  return $permissions;
}
