api = 2
core = 7

; HybridAuth
projects[hybridauth][type] = module

; HybridAuth library
libraries[hybridauth][download][type] = file
libraries[hybridauth][download][url] = https://github.com/hybridauth/hybridauth/archive/master.zip
libraries[hybridauth][download][subtree] = hybridauth-master/hybridauth
