<?php
/**
 * @file
 * Code for the DruTACT - Media feature.
 */

include_once 'drutact_media.features.inc';

/**
 * Implements hook_update_projects_alter().
 */
function drutact_media_update_projects_alter(&$projects)
{
  unset($projects['drutact_media']);
}

/**
 * Implements hook_l10n_update_projects_alter().
 */
function drutact_media_l10n_update_projects_alter(&$projects)
{
  unset($projects['drutact_media']);
}

/**
 * Implements hook_theme_registry_alter().
 * @param $theme_registry
 */
function drutact_media_theme_registry_alter(&$theme_registry)
{
  // Use custom, stripped-down version of file_entity template
  $module_path = drupal_get_path('module', 'drutact_media');
  $theme_registry['file_entity']['theme_path'] = $module_path;
  $theme_registry['file_entity']['template'] = $module_path . '/templates/file_entity';
}

/**
 * Replace media container <div> with <span>
 */
function drutact_media_media_wysiwyg_token_to_markup_alter(&$element, $tag_info, $settings) {
  if (!isset($settings['wysiwyg'])) {
    $element['content']['#type'] = 'markup';
    $attributes = isset($element['content']['#attributes']) ? drupal_attributes($element['content']['#attributes']) : '';
    $element['content']['#prefix'] = '<span ' . $attributes . '>';
    $element['content']['#suffix'] = '</span>';
  }
}

/**
* Implements hook_entity_info_alter().
*/
function drutact_media_entity_info_alter(&$entity_info) {
  $entity_info['file']['view modes']['full_width'] = array(
    'label' => t('Full-width'),
    'custom settings' => TRUE,
  );
  $entity_info['file']['view modes']['linked_preview'] = array(
    'label' => t('Linked preview'),
    'custom settings' => TRUE,
  );

}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drutact_media_form_file_entity_edit_alter(&$form, &$form_state, $form_id) {
  // Make sure the title field is always on top
  $form['filename_field']['#weight'] = -10;
  // Move File access options outside vertical tabs
  $form['destination']['#type'] = 'container';
  $form['destination']['scheme']['#description'] = t('Public files are accessible to everyone, while access to private files must be explicitly granted (i.e. through group membership).');
  $form['destination']['#weight'] = 0;
  unset($form['destination']['#group']);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drutact_media_form_file_entity_add_upload_alter(&$form, &$form_state, $form_id) {
  // Customize the File access options list
  if ($form['#step'] == 3) {
    $form['scheme']['#options']['public'] = t('Public files');
    $form['scheme']['#options']['private'] = t('Private files');
    $form['scheme']['#description'] = t('Public files are accessible to everyone, while access to private files must be explicitly granted (i.e. through group membership).');
  }
}
