<?php
/**
 * @file
 * drutact_media.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drutact_media_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drutact_media_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function drutact_media_image_default_styles() {
  $styles = array();

  // Exported image style: full_width.
  $styles['full_width'] = array(
    'label' => 'Full width (100%)',
    'effects' => array(
      3 => array(
        'name' => 'image_dimensions',
        'data' => array(),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
