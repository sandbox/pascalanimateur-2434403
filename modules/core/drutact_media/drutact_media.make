api = 2
core = 7

; Media
projects[file_entity][type] = module
projects[file_entity][patch][2345741] = https://www.drupal.org/files/issues/file_entity-add_permanent_temporary_actions.patch
projects[file_entity][patch][2473395] = https://www.drupal.org/files/issues/file_entity-rename_destination_access_options-2473395-1.patch
projects[media][type] = module
projects[media][patch][2301883] = https://www.drupal.org/files/issues/media_wysiwyg-fix_html_strip-0.patch
projects[media][patch][1268116] = https://www.drupal.org/files/issues/media-fix_missing_placeholder_regex-1268116-151.patch

; Image
projects[image_dimensions][type] = module
projects[imagemagick][type] = module
projects[iss][type] = module

; Libav
projects[libav][type] = module
projects[libav][download][type] = git
projects[libav][download][url] = http://git.drupal.org/sandbox/PascalAnimateur/2377753.git
projects[libav][download][branch] = 7.x-1.x

; PDF
projects[pdf][type] = module
projects[pdfpreview][type] = module
projects[pdfpreview][version] = 2.x-dev
projects[pdfpreview][patch][2252841] = https://www.drupal.org/files/issues/pdfpreview-flush_image_cache-2252841-5.patch
projects[pdfpreview][patch][2315257] = https://www.drupal.org/files/issues/add_option_to_link_image_to_pdf_file-2315257-9.patch
projects[pdfpreview][patch][2490766] = https://www.drupal.org/files/issues/pdfpreview-remove_widht_height_values.patch

; Libraries
libraries[pdf.js][download][type] = file
libraries[pdf.js][download][url] = https://github.com/mozilla/pdf.js/releases/download/v1.0.907/pdfjs-1.0.907-dist.zip
