api = 2
core = 7

; Breadcrumbs
projects[breadcrumbs_by_path][type] = module

; Metatag
projects[metatag][type] = module

; Path alias
projects[pathauto][type] = module
projects[subpathauto][type] = module
projects[subpathauto][patch][1814516] = https://www.drupal.org/files/issues/subpathauto-not_loading_admin_theme-1814516-18.patch
projects[transliteration][type] = module

; Redirection
projects[globalredirect][type] = module
projects[globalredirect][patch][2385415] = https://www.drupal.org/files/issues/globalredirect-prevent_missing_define-2385415.patch
projects[redirect][type] = module
