<?php
/**
 * @file
 * drutact_performance.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drutact_performance_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'drupal_http_request_function';
  $strongarm->value = 'httprl_override_core';
  $export['drupal_http_request_function'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_background_callback';
  $strongarm->value = 1;
  $export['httprl_background_callback'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_connect_timeout';
  $strongarm->value = '5';
  $export['httprl_connect_timeout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_dns_timeout';
  $strongarm->value = '5';
  $export['httprl_dns_timeout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_global_timeout';
  $strongarm->value = '120';
  $export['httprl_global_timeout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_server_addr';
  $strongarm->value = '';
  $export['httprl_server_addr'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_timeout';
  $strongarm->value = '30';
  $export['httprl_timeout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'httprl_ttfb_timeout';
  $strongarm->value = '20';
  $export['httprl_ttfb_timeout'] = $strongarm;

  return $export;
}
