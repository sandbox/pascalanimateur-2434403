api = 2
core = 7

; Linkit
projects[linkit][type] = module
projects[linkit][patch][2395651] = https://www.drupal.org/files/issues/linkit-display_user_picture-2395651-5.patch

; CKEditor media integration
projects[ckeditor_media][version] = 1.x-dev
projects[ckeditor_media][patch][2458717] = https://www.drupal.org/files/issues/ckeditor_media-wysiwyg_module_support-2458717-2.patch

; Pathologic
projects[pathologic][type] = module

; WYSIWYG
projects[wysiwyg][type] = module
projects[wysiwyg][version] = 2.x-dev

; CKEditor library
libraries[ckeditor][download][type] = file
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_full.zip

; CKEditor plugins
libraries[autogrow][download][type] = file
libraries[autogrow][download][url] = http://download.ckeditor.com/autogrow/releases/autogrow_4.4.7.zip
libraries[autogrow][destination] = libraries/ckeditor/plugins
libraries[image2][download][type] = file
libraries[image2][download][url] = http://download.ckeditor.com/image2/releases/image2_4.4.7.zip
libraries[image2][patch][2400455] = https://www.drupal.org/files/issues/image2-preserve_media_element_class-2400455-2.patch
libraries[image2][destination] = libraries/ckeditor/plugins
libraries[lineutils][download][type] = file
libraries[lineutils][download][url] = http://download.ckeditor.com/lineutils/releases/lineutils_4.4.7.zip
libraries[lineutils][destination] = libraries/ckeditor/plugins
libraries[widget][download][type] = file
libraries[widget][download][url] = http://download.ckeditor.com/widget/releases/widget_4.4.7.zip
libraries[widget][destination] = libraries/ckeditor/plugins
