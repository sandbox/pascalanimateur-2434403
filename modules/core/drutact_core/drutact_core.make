api = 2
core = 7

; Backup and Migrate
projects[backup_migrate][type] = module

; Block export
projects[blockexport][type] = module

; Dynamic environment switching
projects[tadaa][type] = module

; Entity
projects[entity][type] = module
projects[entityreference][type] = module
projects[entityreference_prepopulate][type] = module
projects[token][type] = module

; Features
projects[ctools][type] = module
projects[diff][type] = module
projects[features][type] = module
projects[features_banish][type] = module
projects[features_override][type] = module
projects[features_package_settings][type] = module
projects[features_translations][type] = module
projects[strongarm][type] = module
projects[role_export][type] = module

; Libraries
projects[libraries][type] = module
projects[jquery_update][type] = module

; Rules
projects[rules][type] = module

; Views
projects[views][type] = module
projects[views_bulk_operations][type] = module
projects[views_bulk_operations][patch][2318273] = https://www.drupal.org/files/issues/views_bulk_operations-fix_hide_action_links-2318273-5.patch
projects[views_data_export][type] = module
