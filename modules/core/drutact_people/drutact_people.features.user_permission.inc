<?php
/**
 * @file
 * drutact_people.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_people_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access flood unblock'.
  $permissions['access flood unblock'] = array(
    'name' => 'access flood unblock',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'flood_unblock',
  );

  // Exported permission: 'access user profiles'.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'administer permissions'.
  $permissions['administer permissions'] = array(
    'name' => 'administer permissions',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: 'administer realname'.
  $permissions['administer realname'] = array(
    'name' => 'administer realname',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'realname',
  );

  // Exported permission: 'administer realname registration'.
  $permissions['administer realname registration'] = array(
    'name' => 'administer realname registration',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'realname_registration',
  );

  // Exported permission: 'administer users'.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'user',
  );

  // Exported permission: 'assign Editor role'.
  $permissions['assign Editor role'] = array(
    'name' => 'assign Editor role',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: 'assign all roles'.
  $permissions['assign all roles'] = array(
    'name' => 'assign all roles',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: 'cancel account'.
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'change own username'.
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'clear import_users_xls feeds'.
  $permissions['clear import_users_xls feeds'] = array(
    'name' => 'clear import_users_xls feeds',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'import import_users_xls feeds'.
  $permissions['import import_users_xls feeds'] = array(
    'name' => 'import import_users_xls feeds',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'select account cancellation method'.
  $permissions['select account cancellation method'] = array(
    'name' => 'select account cancellation method',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'user',
  );

  // Exported permission: 'tamper import_users_xls'.
  $permissions['tamper import_users_xls'] = array(
    'name' => 'tamper import_users_xls',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'unlock import_users_xls feeds'.
  $permissions['unlock import_users_xls feeds'] = array(
    'name' => 'unlock import_users_xls feeds',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'feeds',
  );

  return $permissions;
}
