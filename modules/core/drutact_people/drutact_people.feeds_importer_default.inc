<?php
/**
 * @file
 * drutact_people.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function drutact_people_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'import_users_xls';
  $feeds_importer->config = array(
    'name' => 'Import users (Excel)',
    'description' => 'Import users from an Excel file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'xls xlsx',
        'direct' => 0,
        'directory' => 'private://feeds/users_xls',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExcelParser',
      'config' => array(
        'all_worksheets' => 0,
        'max_rows' => '100',
        'chunk_size' => 50,
        'use_chunk_reader' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserProcessor',
      'config' => array(
        'roles' => array(
          200153887 => 0,
          30037204 => 0,
        ),
        'status' => '1',
        'defuse_mail' => 0,
        'user_roles_create' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'first_name',
            'target' => 'field_full_name:given',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'last_name',
            'target' => 'field_full_name:family',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'login_name',
            'target' => 'name',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'password',
            'target' => 'pass',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'email',
            'target' => 'mail',
            'unique' => 1,
          ),
          5 => array(
            'source' => 'roles',
            'target' => 'roles_list',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 1,
        'bundle' => 'user',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['import_users_xls'] = $feeds_importer;

  return $export;
}
