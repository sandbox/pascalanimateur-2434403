<?php
/**
 * @file
 * drutact_import.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_import_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer feeds'.
  $permissions['administer feeds'] = array(
    'name' => 'administer feeds',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'administer feeds_tamper'.
  $permissions['administer feeds_tamper'] = array(
    'name' => 'administer feeds_tamper',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'feeds xls allow download of all entities'.
  $permissions['feeds xls allow download of all entities'] = array(
    'name' => 'feeds xls allow download of all entities',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'feeds_xls',
  );

  return $permissions;
}
