api = 2
core = 7

; Feeds
projects[feeds][type] = module
projects[feeds][patch][2470876] = https://www.drupal.org/files/issues/feeds-delete_example_features-2470876-9.patch
projects[feeds][patch][608408] = https://www.drupal.org/files/issues/feeds-drush_integration-608408-103.patch
projects[feeds_profile2][type] = module
projects[feeds_tamper][type] = module
projects[feeds_tamper_string2id][type] = module
projects[feeds_xls][version] = 1.x-dev
projects[field_collection_feeds][type] = module
projects[job_scheduler][type] = module

; PHPExcel library
libraries[PHPExcel][download][type] = file
libraries[PHPExcel][download][url] = http://download-codeplex.sec.s-msft.com/Download/Release?ProjectName=phpexcel&DownloadId=809023&FileTime=130382506270130000&Build=21029
libraries[PHPExcel][download][subtree] = Classes

; Simplepie library
libraries[simplepie][download][type] = file
libraries[simplepie][download][url] = http://simplepie.org/downloads/simplepie_1.3.1.compiled.php
libraries[simplepie][download][filename] = simplepie.compiled.php
