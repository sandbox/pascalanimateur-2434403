api = 2
core = 7

; Administration interface
projects[admin_menu][type] = module
projects[admin_menu][patch][2541514] = https://www.drupal.org/files/issues/admin_menu-wide_submenu.patch
projects[admin_views][type] = module
projects[adminimal_admin_menu][type] = module
projects[adminimal_admin_menu][patch][2541514] = https://www.drupal.org/files/issues/adminimal_admin_menu-wide_submenu.patch
projects[sticky_edit_actions][type] = module
projects[sticky_edit_actions][patch][2320309] = https://www.drupal.org/files/issues/2320309-Fix_shiny_admin_theme.patch

; Autocomplete deluxe
projects[autocomplete_deluxe][type] = module

; Date popup
projects[date_popup_authored][type] = module

; Font Awesome
projects[fontawesome][type] = module
libraries[fontawesome][download][type] = file
libraries[fontawesome][download][url] = http://fortawesome.github.io/Font-Awesome/assets/font-awesome-4.4.0.zip

; Menu
projects[menu_expanded][type] = module
projects[menu_select][version] = 1.x-dev

; Module filter
projects[module_filter][type] = module

; Options element
projects[options_element][type] = module

; Panels
projects[panels][type] = module
projects[panels_bootstrap_layouts][type] = module
projects[panels_bootstrap_styles][version] = 1.x-dev

; Select or other
projects[select_or_other][type] = module

; Shiny administration theme
projects[shiny][type] = theme
projects[shiny][patch][2362771] = https://www.drupal.org/files/issues/shiny-edit_actions_no_float.patch
projects[shiny][patch][2505485] = https://www.drupal.org/files/issues/shiny-fix_input_form_submit_padding.patch

; Tagadelic
projects[tagadelic][type] = module
projects[tagadelic_views][type] = module

; Vertical tabs
projects[vertical_tabs_responsive][type] = module

; Views bootstrap
projects[views_bootstrap][version] = 3.x-dev

; Views tree
projects[views_tree][type] = module
