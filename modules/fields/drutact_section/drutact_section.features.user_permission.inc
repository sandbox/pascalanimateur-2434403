<?php
/**
 * @file
 * drutact_section.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_section_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'delete terms in sections'.
  $permissions['delete terms in sections'] = array(
    'name' => 'delete terms in sections',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in sections'.
  $permissions['edit terms in sections'] = array(
    'name' => 'edit terms in sections',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
