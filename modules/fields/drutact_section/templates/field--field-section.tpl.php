<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><i class="fa fa-bookmark" title="<?php print t('Section'); ?>"></i>&nbsp;</div>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php if ($item = reset($items)): ?>
      <div class="field-item label label-primary"<?php print $item_attributes[0]; ?>><?php print render($item); ?></div>
    <?php endif; ?>
  </div>
</div>
