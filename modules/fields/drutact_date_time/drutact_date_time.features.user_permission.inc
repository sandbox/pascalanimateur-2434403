<?php
/**
 * @file
 * drutact_date_time.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_date_time_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view date repeats'.
  $permissions['view date repeats'] = array(
    'name' => 'view date repeats',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'date_repeat_field',
  );

  return $permissions;
}
