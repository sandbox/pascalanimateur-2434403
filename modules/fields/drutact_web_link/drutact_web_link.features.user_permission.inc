<?php
/**
 * @file
 * drutact_web_link.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function drutact_web_link_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access advanced_link autocomplete'.
  $permissions['access advanced_link autocomplete'] = array(
    'name' => 'access advanced_link autocomplete',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Editor' => 'Editor',
    ),
    'module' => 'advanced_link',
  );

  return $permissions;
}
