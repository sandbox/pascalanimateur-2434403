api = 2
core = 7

; Drupal 7 core
projects[drupal][version] = 7.41

; Partial word search
projects[drupal][patch][498752] = https://drupal.org/files/search-partial_words_hack-498752-19.patch

; Truncate username above 50 characters instead of 20
projects[drupal][patch][2327267] = https://www.drupal.org/files/issues/username_no_truncate.patch

; Search box auto size
projects[drupal][patch][2456947] = https://www.drupal.org/files/issues/drupal-search_box_size_auto.patch

; User picture filename from user
projects[drupal][patch][2462425] = https://www.drupal.org/files/issues/user-picture_filename_from_user.patch
