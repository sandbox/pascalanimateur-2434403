<?php
/**
 * @file
 * Enables modules and site configuration for a DruTACT site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drutact_form_install_configure_form_alter(&$form, $form_state)
{
  // Provide default values for install configuration
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  $form['site_information']['site_mail']['#default_value'] = 'info@' . $_SERVER['SERVER_NAME'];
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'admin@' . $_SERVER['SERVER_NAME'];
  $form['server_settings']['site_default_country']['#default_value'] = 'CA';
  unset($form['update_notifications']['update_status_module']['#default_value'][1]);
}

/**
 * Implements hook_install_tasks().
 */
function drutact_install_tasks($install_state)
{
  $tasks = array();
  $tasks['drutact_cleanup_installation'] = array(
    'display_name' => st('Cleanup installation'),
    'display' => FALSE,
    'type' => 'normal'
  );
  return $tasks;
}

/**
 * Cleanup installation task.
 */
function drutact_cleanup_installation()
{
  features_revert();
}


/**
 * Implements hook_potx_file_location_info().
 */
function drutact_potx_file_location_info() {
  return array(
    'drutact' => array(
      'path' => drupal_get_path('profile', 'drutact') . '/translations',
    ),
  );
}
