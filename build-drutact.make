api = 2
core = 7

; Drupal 7 core
includes[] = drupal-org-core.make

; DruTACT - Core
includes[] = modules/core/drutact_core/drutact_core.make
includes[] = modules/core/drutact_email/drutact_email.make
includes[] = modules/core/drutact_import/drutact_import.make
includes[] = modules/core/drutact_l10n/drutact_l10n.make
includes[] = modules/core/drutact_media/drutact_media.make
includes[] = modules/core/drutact_people/drutact_people.make
includes[] = modules/core/drutact_performance/drutact_performance.make
includes[] = modules/core/drutact_seo/drutact_seo.make
includes[] = modules/core/drutact_taxonomy/drutact_taxonomy.make
includes[] = modules/core/drutact_ui/drutact_ui.make
includes[] = modules/core/drutact_wysiwyg/drutact_wysiwyg.make

; DruTACT - Content types
includes[] = modules/content_types/drutact_event/drutact_event.make
includes[] = modules/content_types/drutact_group/drutact_group.make
includes[] = modules/content_types/drutact_newsletter/drutact_newsletter.make
includes[] = modules/content_types/drutact_webform/drutact_webform.make

; DruTACT - Environments
includes[] = modules/environments/drutact_dev/drutact_dev.make
includes[] = modules/environments/drutact_prod/drutact_prod.make

; DruTACT - Extra
includes[] = modules/extra/drutact_analytics/drutact_analytics.make
includes[] = modules/extra/drutact_charts/drutact_charts.make
includes[] = modules/extra/drutact_disqus/drutact_disqus.make
includes[] = modules/extra/drutact_email_log/drutact_email_log.make
includes[] = modules/extra/drutact_email_track/drutact_email_track.make
includes[] = modules/extra/drutact_social_login/drutact_social_login.make

; DruTACT - Field types
includes[] = modules/fields/drutact_color/drutact_color.make
includes[] = modules/fields/drutact_date_time/drutact_date_time.make
includes[] = modules/fields/drutact_web_link/drutact_web_link.make

; DruTACT - Themes
includes[] = themes/drutact_bootstrap/drutact_bootstrap.make
