#!/bin/bash

##### CONFIGURATION #####
if [ -e $(dirname $0)/config.sh ] ; then
  . $(dirname $0)/config.sh
else
  echo -e "[`basename $0`] ${MESSAGE_INDENT}\e[91mERROR:\e[0m DruTACT configuration script not found: \e[36m$(dirname $0)/config.sh\e[0m"
  exit 1
fi

cd $DRUPAL_ROOT/sites
for SITE in */ ; do
  if [ -e $SITE/settings.php ] ; then
    cd $SITE
    MESSAGE "Importing translations from code..."
    drush en potx_exportables -y >> $LOG_FILE 2>&1
    drush potx-import-all >> $LOG_FILE 2>&1
    MESSAGE "Clearing all caches..."
    drush cc all >> $LOG_FILE 2>&1
    cd ..
  fi
done
