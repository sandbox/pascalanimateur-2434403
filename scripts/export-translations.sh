#!/bin/bash

##### CONFIGURATION #####
if [ -e $(dirname $0)/config.sh ] ; then
  . $(dirname $0)/config.sh
else
  echo -e "[`basename $0`] ${MESSAGE_INDENT}\e[91mERROR:\e[0m DruTACT configuration script not found: \e[36m$(dirname $0)/config.sh\e[0m"
  exit 1
fi

cd $DRUPAL_ROOT/sites
for SITE in */ ; do
  if [ -e $SITE/settings.php ] ; then
    cd $SITE
    MESSAGE "Updating localized fields context..."
    drush localize-fields profiles/drutact --usecontext -y >> $LOG_FILE 2>&1
    MESSAGE "Exporting translations..."
    drush en potx -y >> $LOG_FILE 2>&1
    drush potx single --folder=profiles/drutact --translations --language=fr >> $LOG_FILE 2>&1
    mv $DRUPAL_ROOT/general.pot $DRUTACT_PATH/translations/drutact.fr.po
    cd ..
  fi
done
