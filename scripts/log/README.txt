# DruTACT scripts log files

Scripts should always log command output by appending " >> $LOG_FILE 2>&1" where
needed. This allows automatic naming of the log file based on the script that
produced the output (ex: log/commit.log).
